//
//  WeatherViewModel.swift
//  Weatharium
//
//  Created by Александр Ветчинов on 30.01.2021.
//

import Foundation
import Combine
import shared


class WeatherViewModel : ObservableObject {
    
    @Published var cityName: String = "Moscow"
    @Published var termperature: String = "----"
    @Published var description: String = ""
    @Published var url: String? = nil
    @Published var errorMessage: String? = nil
    @Published var isLoading = false
    
    private let useCase: WeathariumUseCase
    
    public init(weatherUseCase: WeathariumUseCase ) {
        self.useCase = weatherUseCase
    }
        
    public func refresh(){
        loadWeatherInner(cityName, false)
    }
    
    public func  loadWeatherByCityName (_ newCityName: String){
        loadWeatherInner(newCityName, true)
    }
     
    
    private func loadWeatherInner(_ city: String, _ forced: Bool){
        isLoading = true
      
        self.useCase.loadWeatherByCity(city: city) { (weather, err) in
            guard(err == nil) else {
                self.errorMessage = err?.localizedDescription ?? "unknown error"
                return
            }
            
            guard (weather != nil) else {
                self.errorMessage = "weather not received"
                return
            }
            
            self.cityName = weather?.city ?? "-"
        
        
            let temp = weather?.temp?.intValue ?? -100
            self.termperature = "\(temp)˚C"
            self.description = weather?.description ?? "-"
            self.url = weather?.imageUrl
            self.isLoading = false
        }
         
    }
    
}
