//
//  WeaterView.swift
//  Weatharium
//
//  Created by Александр Ветчинов on 30.01.2021.
//

import SwiftUI
import SDWebImageSwiftUI

private let DEFAULT_IMAGE_URL = "https://live.staticflickr.com/4049/4287207361_88272dbdff.jpg"

@available(iOS 14.0, *)
struct WeatherView: View {
    
    @ObservedObject var model : WeatherViewModel =  Injector.instance.weatherViewModel
    
    @State var isEditMode = false
    @State var newCityName: String = ""
    
    @State var isLoading: Bool = true
    
    let url = URL(string:DEFAULT_IMAGE_URL)!
    var body: some View {
        ZStack {
            VStack{
                if (model.url == nil){
                    Text("Loading")
                        .frame(width: UIScreen.main.bounds.size.width, height: 200, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                } else {
                    imageFrame
                }
                
                cityTextField
                
                Text(model.cityName)
                    .font(.largeTitle)
                    .padding()
                    .onTapGesture {
                        isEditMode = true
                    }.isVisiblie(!isEditMode)
                
                Text(model.termperature)
                    .font(.system(size: 70))
                Spacer()
                
                Text(model.description)
                    .font(.headline)
                
            }
            .onAppear(perform: model.refresh)
            .navigationBarTitle("")
            .navigationBarBackButtonHidden(true)
            .navigationBarHidden(true)
            
            progressView
        }
    }
    
    var cityTextField: some View {
        return  HStack {
            TextField("type the city name", text: $newCityName, onCommit: { print("commit") })

            Button("Apply"){
                guard !newCityName.isEmpty else {return}
                model.loadWeatherByCityName(newCityName)
                newCityName = ""
                isEditMode = false
            }
        }.padding()
        .isVisiblie(isEditMode)
    }
    
    var imageFrame: some View {
        return WebImage(url: URL(string:model.url!)!)
            .placeholder(Image(systemName: "photo"))
            .resizable()
            .transition(.fade(duration: 0.5))
            .scaledToFill()
            .frame(width: UIScreen.main.bounds.size.width, height: 250, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            .clipped()
    }
    
    @available(iOS 14.0, *)
    var progressView: some View {
        return  ZStack {
            Color.black.opacity(0.25)
                .ignoresSafeArea()
            ProgressView()
                .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
        }
        .isVisiblie(model.isLoading)
    }
}

@available(iOS 14.0, *)
struct WeatherView_Previews: PreviewProvider {
    static var previews: some View {
        WeatherView( )
    }
}
