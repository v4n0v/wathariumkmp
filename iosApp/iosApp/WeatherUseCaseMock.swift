//
//  WeatherUseCaseMock.swift
//  iosApp
//
//  Created by v4n0v on 21.04.2021.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation
import shared

class WeatherUseCaseMock : WeathariumUseCase {
    func loadWeatherByCity(city: String, completionHandler: @escaping (WeatherModel?, Error?) -> Void) {
        completionHandler(nil, nil)
    }
    
    func updatePhotoUrl(city: String, completionHandler: @escaping (String?, Error?) -> Void) {
        completionHandler(nil, nil)
    }
    
    
}
