
import SwiftUI
import CoreData



@main
struct WeathariumApp: App {
   
    @available(iOS 14.0, *)
   var body: some Scene {
        WindowGroup {
              WeatherView()
        }
    }
}
 
