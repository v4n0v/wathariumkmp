//
//  ViewExtensions.swift
//  Weatharium
//
//  Created by Александр Ветчинов on 25.02.2021.
//

import Foundation
import SwiftUI



extension View {
    func navigate<NewView: View>(to view: NewView, when binding: Binding<Bool>) -> some View {
        NavigationView {
            ZStack {
                self
                    .navigationBarTitle("")
                    .navigationBarHidden(true)

                NavigationLink(
                    destination: view
                        .navigationBarTitle("")
                        .navigationBarHidden(true),
                    isActive: binding
                ) {
                    EmptyView()
                }
            }
        }
    }
    
     
        @ViewBuilder func isHidden(_ hidden: Bool, remove: Bool = false) -> some View {
            if hidden {
                if !remove {
                    self.hidden()
                }
            } else {
                self
            }
        }
   
        @ViewBuilder func isVisiblie(_ isVisible: Bool, remove: Bool = false) -> some View {
            if !isVisible {
                if !remove {
                    self.hidden()
                }
            } else {
                self
            }
        }
}


