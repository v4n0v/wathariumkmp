//
//  StringExtesions.swift
//  Weatharium
//
//  Created by Александр Ветчинов on 31.01.2021.
//

import Foundation

extension String {
    func toUrl()->URL {
        let urlString = self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        return URL(string: urlString)!
    }
}
