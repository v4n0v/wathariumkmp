//
//  ContentViewModel.swift
//  iosApp
//
//  Created by v4n0v on 02.04.2021.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation
import shared

private let  url = "https://jsonplaceholder.typicode.com/todos/1"

class ContentViewModel: ObservableObject {
    
    @Published var title: String = ""
    
    private let service:TestDataService = NetworkModule().getTestService(baseUrl: url)

    func testCall(){
        title = greet()
        service.getTestData { (data, err) in
            guard (err == nil) else {
                self.title = "error \(err?.localizedDescription)"
                return
            }
            self.title = data?.title ?? "шота нихуя"
        }
    }
         
    private func greet() -> String {
        return Greeting().greeting()
    }
    
}
