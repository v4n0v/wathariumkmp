//
//  Injector.swift
//  Weatharium
//
//  Created by Александр Ветчинов on 17.02.2021.
//

import Foundation
import shared


class Injector {
    static let instance = Injector()
    
    lazy var weatherUseCase = {NetworkModule().weathariumUseCase }()
    
    lazy var weatherViewModel = {
        WeatherViewModel(weatherUseCase: weatherUseCase)
    }()
    
//    lazy var cache: WeatherCache = {
//        CoreDataCache()
//    }()
}
