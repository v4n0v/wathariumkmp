buildscript {
    val kotlinVersion: String by project
    val serializationVersion: String by project

    repositories {
        gradlePluginPortal()
        jcenter()
        google()
        mavenCentral()
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion")
//        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.4.30")
        classpath("com.android.tools.build:gradle:4.2.1")

        classpath(kotlin("serialization", version = kotlinVersion))
//        classpath ("org.jetbrains.kotlin:kotlin-serialization:$kotlinVersion")
//        classpath ("com.squareup.sqldelight:gradle-plugin:$sqldelightVersion")
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        mavenCentral()
        maven (url ="https://dl.bintray.com/kotlin/kotlinx" )
        maven (url = "https://dl.bintray.com/kotlin/ktor" )
    }
}