plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
//    kotlin("android-extensions")
//    id("kotlin-kapt")
}



android {
    compileSdkVersion(30)

    defaultConfig {
        applicationId = "com.ezmobdev.weatariumkmp.androidApp"
        minSdkVersion(24)
        targetSdkVersion(30)
        versionCode = 1
        versionName = "1.0"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
        }
    }

    buildFeatures {
        viewBinding = true
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

    buildToolsVersion = "30.0.3"
}

dependencies {
    val coroutinesVersion: String by project
    val koinVersion: String by project
    val viewModelVersion: String by project
    val navigationVersion: String by project

    implementation(project(":shared"))

    implementation ("androidx.core:core-ktx:1.3.2")
    implementation("com.google.android.material:material:1.2.1")
    implementation("androidx.appcompat:appcompat:1.2.0")
    implementation("androidx.constraintlayout:constraintlayout:2.0.2")

    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutinesVersion")

    implementation ("androidx.navigation:navigation-fragment-ktx:$navigationVersion")
    implementation ("androidx.navigation:navigation-ui-ktx:$navigationVersion")
    implementation ("com.android.databinding:viewbinding:4.1.3")

    // koin
    implementation ("org.koin:koin-android-viewmodel:$koinVersion")
    implementation ("org.koin:koin-android:$koinVersion")
    implementation ("org.koin:koin-core:$koinVersion")

    //view model
    implementation ("androidx.lifecycle:lifecycle-viewmodel-ktx:$viewModelVersion")

//    kapt ("com.android.databinding:compiler:3.1.4")
//    kapt ("com.android.databinding:compiler:$android_plugin_version")
}