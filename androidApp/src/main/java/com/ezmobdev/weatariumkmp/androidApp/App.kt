package com.ezmobdev.weatariumkmp.androidApp

import android.app.Application
import com.ezmobdev.weatariumkmp.androidApp.di.appModule
import com.ezmobdev.weatariumkmp.androidApp.di.testModule
import com.ezmobdev.weatariumkmp.androidApp.di.useCaseModule
import com.ezmobdev.weatariumkmp.androidApp.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App: Application() {

    companion object {
       private val MODULES =  listOf(useCaseModule, viewModelModule, appModule, testModule)
    }

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            androidLogger()
            modules(MODULES)
        }
    }

}