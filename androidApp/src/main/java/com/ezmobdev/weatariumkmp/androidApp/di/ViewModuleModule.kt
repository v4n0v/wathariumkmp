package com.ezmobdev.weatariumkmp.androidApp.di

import com.ezmobdev.weatariumkmp.androidApp.screens.home.HomeViewModel
import com.ezmobdev.weatariumkmp.androidApp.screens.splash.SplashViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule  = module {
    viewModel { SplashViewModel(get()) }
    viewModel { HomeViewModel(get(), get()) }
}