package com.ezmobdev.weatariumkmp.androidApp.screens.splash

import androidx.lifecycle.MutableLiveData
import com.ezmobdev.weatariumkmp.androidApp.screens.BaseViewModel
import com.ezmobdev.weatariumkmp.shared.data.WeatherModel
import com.ezmobdev.weatariumkmp.shared.usecase.WeathariumUseCase
import kotlinx.coroutines.launch

class SplashViewModel(private val weatherUseCase: WeathariumUseCase) : BaseViewModel() {

    val preloadedData: MutableLiveData<WeatherModel> by lazy { MutableLiveData<WeatherModel>() }

    override fun onCreated() {
        super.onCreated()
        scope.launch {
            val weather = weatherUseCase.loadWeatherByCity("Moscow")
            preloadedData.value = weather
        }
    }

}