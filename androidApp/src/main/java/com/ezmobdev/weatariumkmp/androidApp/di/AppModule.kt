package com.ezmobdev.weatariumkmp.androidApp.di

import com.ezmobdev.weatariumkmp.androidApp.cache.Cache
import com.ezmobdev.weatariumkmp.androidApp.cache.CacheImpl
import org.koin.dsl.module

val appModule = module {
    single<Cache> { CacheImpl() }
}