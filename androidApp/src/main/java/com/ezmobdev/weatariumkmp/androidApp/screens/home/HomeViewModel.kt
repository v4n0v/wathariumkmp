package com.ezmobdev.weatariumkmp.androidApp.screens.home

import androidx.lifecycle.MutableLiveData
import com.ezmobdev.weatariumkmp.androidApp.cache.Cache
import com.ezmobdev.weatariumkmp.androidApp.screens.BaseViewModel
import com.ezmobdev.weatariumkmp.androidApp.utils.dateFormat
import com.ezmobdev.weatariumkmp.androidApp.utils.getWeatherIcon
import com.ezmobdev.weatariumkmp.shared.data.WeatherModel
import com.ezmobdev.weatariumkmp.shared.usecase.WeathariumUseCase
import kotlinx.coroutines.launch
import java.lang.Exception
import java.util.*

class HomeViewModel(private val weathariumUseCase: WeathariumUseCase, private val cache: Cache) :
    BaseViewModel() {

    val imageData: MutableLiveData<String> by lazy { MutableLiveData<String>() }
    val weatherData: MutableLiveData<WeatherModel> by lazy { MutableLiveData<WeatherModel>() }
    val iconWeatherData: MutableLiveData<Int> by lazy { MutableLiveData<Int>() }
    val isPhotoLoading: MutableLiveData<Boolean> by lazy { MutableLiveData<Boolean>() }
    val dateField: MutableLiveData<String> by lazy { MutableLiveData<String>() }

    fun onNewWeather(weather: WeatherModel) {
        weatherData.value = weather
        iconWeatherData.value = getWeatherIcon(weather.id)
        weather.imageUrl?.let { url -> imageData.value = url }
        dateField.value = dateFormat().format(Date().time)
        isPhotoLoading.value = false
    }

    fun refresh(city: String? = null) {
        isPhotoLoading.value = true
        scope.launch {
            try {
                val weather = weathariumUseCase.loadWeatherByCity(city ?: cache.city)
                onNewWeather(weather)
            } catch (e: Exception) {
                errorData.value = e
            }
        }
    }

    fun updatePhoto() {
        isPhotoLoading.value = true
        scope.launch {
            try {
                weathariumUseCase.updatePhotoUrl(cache.city)?.let { url -> imageData.value = url }
                isPhotoLoading.value = false
            } catch (e: Exception) {
                errorData.value = e
            }
        }
    }


}