package com.ezmobdev.weatariumkmp.androidApp.di

import com.ezmobdev.weatariumkmp.shared.NetworkModule
import com.ezmobdev.weatariumkmp.shared.services.testService.TestDataService
import org.koin.dsl.module

val testModule = module {
    single<TestDataService> { NetworkModule.testService }
}