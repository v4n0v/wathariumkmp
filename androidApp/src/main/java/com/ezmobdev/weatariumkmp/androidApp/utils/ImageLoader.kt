package com.ezmobdev.weatariumkmp.androidApp.utils

import android.content.Context
import android.graphics.*
import android.os.Handler
import android.os.Looper
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.ImageView
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.net.URL

internal object ImageLoader {
    internal fun ImageView.load(url: String ) = ignoreException {
        val localBmp = context?.getAvatarCachedByUrl(url)
        if (localBmp != null) {
            setImageBitmap(localBmp)
            return@ignoreException
        }

        Log.d("ImageLoader", "init job for load $url" )
        val job = Thread {
            try {
                Log.d("ImageLoader", "start load $url" )
                val bmp = BitmapFactory.decodeStream(URL(url).openConnection().getInputStream())
                Handler(Looper.getMainLooper()).post {
//                    context?.saveAvatarByUrl(url, bmp)
                    setImageBitmap(bmp)
                }
            } catch(e: Exception){
                Log.d("ImageLoader", "load $url error", e)
            }
        }
        job.start()
        addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
            override fun onViewAttachedToWindow(view: View) = job.start()

            override fun onViewDetachedFromWindow(view: View) {
                if (!job.isInterrupted) job.interrupt()
            }
        })
    }

    private fun Context.getAvatarCachedByUrl(url: String): Bitmap? {
        val sJson = getUserCachedAvatarJson()
        if (sJson.isNullOrBlank()) return null

        val json = JSONObject(sJson)
        val base64Bmp = try {
            json.get(url).toString()
        } catch (e: Exception) {
            null
        }
        if (base64Bmp.isNullOrBlank()) return null

        val decodedString: ByteArray = Base64.decode(base64Bmp, Base64.DEFAULT)
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
    }

    private fun Context.saveAvatarByUrl(url: String, avatar: Bitmap) {
        val sJson = getUserCachedAvatarJson()
        val json = JSONObject(sJson ?: "{}")
        val byteArrayOutputStream = ByteArrayOutputStream()
        avatar.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream)
        val byteArray: ByteArray = byteArrayOutputStream.toByteArray()
        val encoded: String = Base64.encodeToString(byteArray, Base64.DEFAULT)
        json.put(url, encoded)
        //save json info shared prefs

    }

    private fun Context.getUserCachedAvatarJson(): String? {
        // val sJson = get from shared prefs
        return null
    }


}