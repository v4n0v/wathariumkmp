package com.ezmobdev.weatariumkmp.androidApp.screens.home

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.view.isVisible
import com.ezmobdev.weatariumkmp.androidApp.R
import com.ezmobdev.weatariumkmp.androidApp.databinding.FragmentHomeBinding
import com.ezmobdev.weatariumkmp.androidApp.screens.BaseFragment
import com.ezmobdev.weatariumkmp.androidApp.screens.splash.SplashFragment.Companion.WEATHER_KEY
import com.ezmobdev.weatariumkmp.androidApp.utils.Animator.positionInAnimation
import com.ezmobdev.weatariumkmp.androidApp.utils.Animator.zoomInAnimation
import com.ezmobdev.weatariumkmp.androidApp.utils.ImageLoader.load
import com.ezmobdev.weatariumkmp.androidApp.utils.getTemperatureBetween
import com.ezmobdev.weatariumkmp.androidApp.utils.temperatureFormat
import com.ezmobdev.weatariumkmp.shared.data.WeatherModel
import org.koin.android.ext.android.inject

class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>() {

    override val viewModel: HomeViewModel by inject()

    override val viewBindingFun: (LayoutInflater) -> FragmentHomeBinding
        get() = { FragmentHomeBinding.inflate(it) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.getString(WEATHER_KEY)?.let {
            viewModel.onNewWeather(WeatherModel.fromJson(it))
        } ?: viewModel.refresh()

        with(binding) {
            toolbarLayout.setExpandedTitleColor(getColor(R.color.transparent))
            toolbarLayout.setCollapsedTitleTextColor(Color.rgb(255, 255, 255))
            header.setOnClickListener { viewModel.updatePhoto() }
        }

        viewModel.iconWeatherData.onData {
            content.infoWeatherIco.setImageResource(it)
        }

        viewModel.imageData.onData {
            header.load(it)
        }

        viewModel.isPhotoLoading.onData {
            progressBar.isVisible = it
            ivProgress.isVisible = it
        }

        viewModel.dateField.onData {
            content.tvLastUpdate.text = it
        }

        viewModel.weatherData.onData { weatherData ->
            with(content) {
                infoPressure.text = weatherData.pressure?.toString() ?: "-"
                infoTemperatureTv.text = temperatureFormat(weatherData.temp?.toInt())
                infoDescriptionTv.text = weatherData.description ?: "-"
                infoHumidity.text = weatherData.humidity?.toString() ?: "-"
                infoMinMax.text =  getTemperatureBetween(weatherData.minTemp?.toInt(), weatherData.maxTemp?.toInt())
                infoWind.text = weatherData.wind?.toString() ?: "-"

                fab.startAnimation(zoomInAnimation(650, offset = 100))
                infoTemperatureTv.startAnimation(
                    positionInAnimation(
                        600f,
                        0f,
                        duration = 650,
                        offset = 200
                    )
                )
                infoWeatherIco.startAnimation(zoomInAnimation(duration = 650, offset = 200))
                infoDescriptionShape.startAnimation(
                    zoomInAnimation(
                        duration = 650,
                        offset = 300
                    )
                )
                infoMinMax.startAnimation(
                    positionInAnimation(
                        600f,
                        0f,
                        duration = 650,
                        offset = 200
                    )
                )
                detalesContainer.startAnimation(
                    positionInAnimation(
                        0F,
                        350.0F,
                        duration = 650,
                        offset = 300
                    )
                )
            }
        }
    }
}