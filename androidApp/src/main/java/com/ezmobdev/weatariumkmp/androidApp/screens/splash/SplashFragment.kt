package com.ezmobdev.weatariumkmp.androidApp.screens.splash

import android.os.Bundle
import android.view.LayoutInflater
import com.ezmobdev.weatariumkmp.androidApp.databinding.FragmentSpalshBinding
import com.ezmobdev.weatariumkmp.androidApp.screens.BaseFragment
import org.koin.android.ext.android.inject
import androidx.navigation.fragment.findNavController
import com.ezmobdev.weatariumkmp.androidApp.R

class SplashFragment : BaseFragment<FragmentSpalshBinding, SplashViewModel>() {

    companion object {
        const val WEATHER_KEY = "weather_key"
    }
    override val viewModel: SplashViewModel by inject()
    override val viewBindingFun: (LayoutInflater) -> FragmentSpalshBinding
        get() = { FragmentSpalshBinding.inflate(it) }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.errorData.onData {
//                showInformDialog("Ошибка", it?.message) {
//                    navigator.closeApp()
//                }
            }

        viewModel.preloadedData.onData {
            val bundle = Bundle().apply {
                putString(WEATHER_KEY, it.toJson())
            }

            findNavController().navigate(R.id.action_splashFragment_to_mainFragment, bundle)
//            findN
//                navigator.navigateTo(Layout.SPLASH_MAIN, "weather" to it.first, "photo" to it.second)
            }
    }


}