package com.ezmobdev.weatariumkmp.androidApp.utils

import android.content.Context
import com.ezmobdev.weatariumkmp.androidApp.R
import java.text.SimpleDateFormat
import java.util.*


const val CELSIUM = "\u00B0"

fun getWeatherIcon(id: Int?): Int {
    var newId = id
    return if (newId == null || newId == 800)
        R.drawable.day_synny
    else {
        newId = (id ?: 0) / 100
        when (newId) {
            2 -> R.drawable.day_thunder
            3 -> R.drawable.day_drizzle
            5 -> R.drawable.day_rainy
            6 -> R.drawable.day_snowie
            7 -> R.drawable.day_foggy
            8 -> R.drawable.day_cloudly
            else -> R.drawable.day_synny
        }
    }
}

fun dateFormat() = SimpleDateFormat("dd.MMM HH:mm", Locale.getDefault())

fun temperatureFormat(temperature: Int?): String {
    if(temperature == null)
        return "N/A"
    return "$temperature$CELSIUM"
}

fun getTemperatureBetween(min: Int?, max: Int?) =
    temperatureFormat(min) + " " + temperatureFormat(max)

fun Context.getWeatherDescription(id: Int?): String {
    val desc = resources.getStringArray(R.array.description_wether)
    return if (id == null)
        desc.last() else desc[id / 100]
}

fun ignoreException(block: () -> Unit) {
    try {
        block()
    } catch (e: Exception) {
    }
}