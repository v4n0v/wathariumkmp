package com.ezmobdev.weatariumkmp.androidApp.utils

import android.view.animation.Animation
import android.view.animation.AnimationSet
import android.view.animation.ScaleAnimation
import android.view.animation.TranslateAnimation


object Animator {

        fun zoomInAnimation(duration: Long = 500, offset: Long = 0): AnimationSet {
            val showAnimationSet = AnimationSet(false)
            showAnimationSet.addAnimation(scaleAnimation(0f, 1f, duration, offset))
//            showAnimationSet.interpolator = MyBounceInterpolator(0.4, 6.0)
            return showAnimationSet
        }


        fun positionInAnimation(x: Float, y: Float, duration: Long = 400, offset: Long = 0): AnimationSet {
            val animationSet = AnimationSet(false)
            animationSet.addAnimation(positionAnimation(x, 0f, y, 0f, duration, offset))
            return animationSet
        }

        private fun positionAnimation(
            xFrom: Float,
            xTo: Float,
            yFrfom: Float,
            yTo: Float,
            duration: Long,
            offset: Long
        ): Animation {
            val animation = TranslateAnimation(xFrom, xTo, yFrfom, yTo)
            animation.duration = duration
            animation.startOffset = offset
            return animation
        }


        private fun scaleAnimation(from: Float, to: Float, duration: Long, offset: Long): Animation {
            val animation = ScaleAnimation(
                from, to, from, to,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f
            )
            animation.duration = duration
            animation.startOffset = offset

            return animation
        }

}

internal class MyBounceInterpolator(amplitude: Double, frequency: Double) : android.view.animation.Interpolator {
    private var mAmplitude = 1.0
    private var mFrequency = 10.0

    init {
        mAmplitude = amplitude
        mFrequency = frequency
    }

    override fun getInterpolation(time: Float): Float {
        return (-1.0 * Math.pow(Math.E, -time / mAmplitude) *
                Math.cos(mFrequency * time) + 1).toFloat()
    }
}