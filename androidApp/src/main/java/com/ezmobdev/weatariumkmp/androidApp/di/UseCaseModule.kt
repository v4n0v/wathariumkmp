package com.ezmobdev.weatariumkmp.androidApp.di

import com.ezmobdev.weatariumkmp.shared.NetworkModule
import com.ezmobdev.weatariumkmp.shared.usecase.WeathariumUseCase
import org.koin.dsl.module

val useCaseModule = module {
    single { NetworkModule.weathariumUseCase }
}