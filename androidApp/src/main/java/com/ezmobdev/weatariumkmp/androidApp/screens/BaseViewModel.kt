package com.ezmobdev.weatariumkmp.androidApp.screens

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelChildren
import kotlin.coroutines.CoroutineContext

open class BaseViewModel: ViewModel()  {

    private var jobTracker: Job = Job()
    protected val scope : CoroutineScope = CoroutineScope(Dispatchers.Main + jobTracker )

    val errorData: MutableLiveData<Throwable> by lazy { MutableLiveData<Throwable>() }

    open fun onCreated(){}

    override fun onCleared() {
        super.onCleared()
        scope.coroutineContext.cancelChildren()
    }



}