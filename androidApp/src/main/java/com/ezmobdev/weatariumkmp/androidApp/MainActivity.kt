package com.ezmobdev.weatariumkmp.androidApp

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.ezmobdev.weatariumkmp.shared.Greeting
import android.widget.TextView
import androidx.navigation.Navigation
import com.ezmobdev.weatariumkmp.shared.NetworkModule
import com.ezmobdev.weatariumkmp.shared.services.testService.TestDataService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject


fun greet(): String {
    return Greeting().greeting()
}

class MainActivity : AppCompatActivity() {

    private var jobTracker: Job = Job()
    private val scope : CoroutineScope = CoroutineScope(Dispatchers.Main + jobTracker )

    private val testService : TestDataService by inject()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        Navigation.findNavController(this, R.id.navigator)



        scope.launch {
            testService.postTestData()
        }
//        tv.text = greet()
    }
}
