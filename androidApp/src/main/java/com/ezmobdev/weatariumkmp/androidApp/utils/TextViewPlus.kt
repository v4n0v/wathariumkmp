package com.ezmobdev.weatariumkmp.androidApp.utils

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.util.Log
import com.ezmobdev.weatariumkmp.androidApp.R


class TextViewPlus : androidx.appcompat.widget.AppCompatTextView {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setCustomFont(context, attrs)
    }
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        setCustomFont(context, attrs)
    }

    private fun setCustomFont(ctx: Context, attrs: AttributeSet) {
        val a = ctx.obtainStyledAttributes(attrs, R.styleable.TextViewPlus)
        val customFont = a.getString(R.styleable.TextViewPlus_customFont)
        setCustomFont(ctx, customFont)
        a.recycle()
    }

    private fun setCustomFont(ctx: Context, asset: String?): Boolean {

        return try {
            typeface = Typeface.createFromAsset(ctx.assets, asset)
            true
        } catch (e: Exception) {
            Log.e("TextViewPlus","Could not get typeface: ${ e.message}")
              false
        }
    }

}
