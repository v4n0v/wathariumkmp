package com.ezmobdev.weatariumkmp.androidApp.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.viewbinding.ViewBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import com.ezmobdev.weatariumkmp.androidApp.R

abstract class BaseFragment<VB : ViewBinding, VM : BaseViewModel> : Fragment() {

    private var _viewBinding: VB? = null
//    private  val _viewModel:Lazy< VM>  by inject()

    abstract val viewModel: VM

    protected val binding: VB
        get() = _viewBinding!!

    abstract val viewBindingFun: (LayoutInflater) -> VB


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _viewBinding = viewBindingFun(inflater)
        return _viewBinding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.onCreated()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _viewBinding = null
    }

    protected fun <T> MutableLiveData<T>.onData(block: VB.(T) -> Unit) {
        observe(viewLifecycleOwner, { block(binding, it) })
    }

    protected fun toast(msg: String) {
        Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
    }

    protected  fun getColor(@ColorRes res: Int) =
        ContextCompat.getColor(requireContext(), res)

}
