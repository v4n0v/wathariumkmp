package com.ezmobdev.weatariumkmp.shared.services.testService

import com.ezmobdev.weatariumkmp.shared.api.client.ApiClient
import com.ezmobdev.weatariumkmp.shared.api.request.Request
import com.ezmobdev.weatariumkmp.shared.data.TestData
import com.ezmobdev.weatariumkmp.shared.data.TestPostData

internal class TestServiceImpl : TestDataService {

    companion object {
        private const val GET_URL = "https://jsonplaceholder.typicode.com/todos/1"
        private const val POST_URL = "https://jsonplaceholder.typicode.com/posts"
        private val POST_DATA = TestPostData(1, "awesome_title", "awesome_body", 1010)
    }

    private val getTestDataRequest: Request by lazy { Request.Get(GET_URL) }

    override suspend fun getTestData(): TestData =
        ApiClient.execute(getTestDataRequest)

    override suspend fun postTestData() {
        val request = Request.Post(POST_URL).apply {
            addBody(POST_DATA)
        }
        println("post request: ${POST_DATA.title}, ${POST_DATA.body}")
        val echo = ApiClient.execute<TestPostData>(request)

        println("post response: ${echo.title}, ${echo.body}")
        if (echo.body != POST_DATA.body) throw Exception()
    }

}