package com.ezmobdev.weatariumkmp.shared.services.testService

import com.ezmobdev.weatariumkmp.shared.api.Service
import com.ezmobdev.weatariumkmp.shared.data.TestData
import com.ezmobdev.weatariumkmp.shared.data.TestPostData


interface TestDataService : Service {
    suspend fun getTestData(): TestData
    suspend fun postTestData()
}

