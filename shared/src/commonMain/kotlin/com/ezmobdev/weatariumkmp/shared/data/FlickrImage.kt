package com.ezmobdev.weatariumkmp.shared.data

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlin.random.Random

@Serializable
class FlickrImage(
    @SerialName("photos")
    val data: FlickrData,
    @SerialName("stat")
    val status: String
) {
    fun getRandomUrl(): String? {
        val safePhotos = data.photos.filter { it.hadData() }
        if (safePhotos.isEmpty()) return null

        val pos = Random.nextInt(0, safePhotos.size)
        val photo = safePhotos[pos]

        return photo.urlM
    }
}

@Serializable
class FlickrData (
    @SerialName("perpage")
    val qty: Int,
    @SerialName("photo")
    val photos: List<FlickrPhoto>
    )

@Serializable
class FlickrPhoto(
    val id: String,
    @SerialName("url_m")
    val urlM: String? = null,
    val width: Int? = null,
    val height: Int?= null
) {
    fun hadData() = urlM != null //&& width != null && height != null
}

