package com.ezmobdev.weatariumkmp.shared.services.weather

import com.ezmobdev.weatariumkmp.shared.data.WeatherRaw



interface WeatherService {
    suspend fun loadWeather(city: String): WeatherRaw
}

