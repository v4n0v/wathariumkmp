package com.ezmobdev.weatariumkmp.shared.services.image

import com.ezmobdev.weatariumkmp.shared.data.FlickrImage

interface ImageService {
    suspend fun getCityPhotosByName(city: String): FlickrImage
}

