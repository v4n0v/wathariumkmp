package com.ezmobdev.weatariumkmp.shared.api.client

import com.ezmobdev.weatariumkmp.shared.api.request.Request
import com.ezmobdev.weatariumkmp.shared.api.request.RequestType
import com.ezmobdev.weatariumkmp.shared.ioDispatcher
import io.ktor.client.*
import io.ktor.client.features.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.features.logging.*
import io.ktor.client.request.*
import io.ktor.content.*
import io.ktor.http.*
import io.ktor.util.*
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json

internal object ApiClient {

    private const val TIMEOUT = 15_000L
    private val LOG_LEVEL = LogLevel.BODY
    private val jsonFormat = Json { ignoreUnknownKeys = true }

    private val client = HttpClient {
        install(JsonFeature) {
            serializer = KotlinxSerializer(jsonFormat)
        }
        install(HttpTimeout) { requestTimeoutMillis = TIMEOUT }
        install(Logging) {
            logger = Logger.SIMPLE
            level = LOG_LEVEL
        }
    }

    @KtorExperimentalAPI
    suspend inline fun <reified T> execute(
        request: Request
    ): T = when (request.requestType) {
        RequestType.GET -> client.get { buildRequest(request) }
        RequestType.POST -> client.post { buildRequest(request) }
        RequestType.PUT -> client.put { buildRequest(request) }
        RequestType.DELETE -> client.delete { buildRequest(request) }
    }


    private fun HttpRequestBuilder.buildRequest(request: Request) {
        header(HttpHeaders.CacheControl, "no-cache")

        url(request.baseUrl)
        request.body?.let {
            body = TextContent(it, contentType = ContentType.Application.Json)
        }

        request.queryParams?.let { params ->
            params.forEach { parameter(it.key, it.value) }
        }
        request.headers?.let { headers ->
            headers.forEach { header(it.key, it.value) }
        }
    }


}