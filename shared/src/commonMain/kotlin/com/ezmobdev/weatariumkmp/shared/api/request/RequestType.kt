package com.ezmobdev.weatariumkmp.shared.api.request

enum class RequestType { GET, POST, PUT, DELETE }