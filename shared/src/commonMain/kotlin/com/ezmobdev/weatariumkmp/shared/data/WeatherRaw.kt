package com.ezmobdev.weatariumkmp.shared.data

import kotlinx.serialization.SerialName

@kotlinx.serialization.Serializable
data class WeatherRaw(
//    val coord: Coord,
    var weather: List<WeatherInfo>?,
    val main: Main,
    val wind: Wind,
    @SerialName("name")
    val city: String
)

@kotlinx.serialization.Serializable
class Wind  {
    var speed: Double? = null
    var deg: Double? = null
}

@kotlinx.serialization.Serializable
data class WeatherInfo(
    val id: Int,
    val main: String,
    val description: String,
    val icon: String
)

@kotlinx.serialization.Serializable
class Main   {
    var temp: Double? = null
    var pressure: Double? = null
    var humidity: Int? = null
    var tempMin: Double? = null
    var tempMax: Double? = null

}
