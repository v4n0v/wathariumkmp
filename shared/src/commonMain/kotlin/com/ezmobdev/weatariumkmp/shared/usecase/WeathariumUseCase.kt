package com.ezmobdev.weatariumkmp.shared.usecase

import com.ezmobdev.weatariumkmp.shared.data.FlickrImage
import com.ezmobdev.weatariumkmp.shared.data.WeatherModel
import com.ezmobdev.weatariumkmp.shared.data.WeatherRaw
import com.ezmobdev.weatariumkmp.shared.services.image.ImageService
import com.ezmobdev.weatariumkmp.shared.services.weather.WeatherService
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope

interface WeathariumUseCase {
    suspend fun loadWeatherByCity(city: String): WeatherModel
    suspend fun updatePhotoUrl(city: String): String?
}

class WeathariumUseCaseImpl(
    private val imageService: ImageService,
    private val weatherService: WeatherService
): WeathariumUseCase{

    override suspend fun loadWeatherByCity(city: String): WeatherModel = coroutineScope {
        val result = awaitAll(async {
            weatherService.loadWeather(city)
        }, async {
            imageService.getCityPhotosByName(city)
        })

        val weatherRaw = (result[0] as WeatherRaw)
        val flickrImage  = (result[1] as FlickrImage)

        val weather = weatherRaw.weather?.firstOrNull()
        val main = weatherRaw.main

        WeatherModel(
            weather?.id ?: 0,
            weatherRaw.city,
            main.temp ,
            main.tempMin,
            main.tempMax,
            main.humidity,
            main.pressure,
            weatherRaw.wind.speed,
            weather?.description  ,
            weather?.icon ,
            flickrImage.getRandomUrl()
        )
    }

    override suspend fun updatePhotoUrl(city: String): String? =
        imageService.getCityPhotosByName(city).getRandomUrl()



}