package com.ezmobdev.weatariumkmp.shared.services.image

import com.ezmobdev.weatariumkmp.shared.api.client.ApiClient
import com.ezmobdev.weatariumkmp.shared.api.request.Request
import com.ezmobdev.weatariumkmp.shared.data.FlickrImage

class FlickrImageService: ImageService {

    companion object {
        private const val BASE_URL = "https://api.flickr.com/services/rest/"
        private const val API_KEY =  "9c6b4a5f6ad93dafa5a5ca0ef3b2f864"
    }

    override suspend fun getCityPhotosByName(city: String): FlickrImage =
        ApiClient.execute(getImageRequest(city))

    private fun getImageRequest(city: String): Request = Request.Get(BASE_URL).apply {
            appendQueryParams {
                put("api_key", API_KEY)
                put("method", "flickr.photos.search")
                put("nojsoncallback", "1")
                put("media", "data")
                put("sort", "relevance")
                put("content_type", "1")
                put("format", "json")
                put("extras", "url_m")
                put("safe_search", "safe")
                put("pr_page", "50")
                put("text", city)
            }
        }

}