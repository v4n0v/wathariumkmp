package com.ezmobdev.weatariumkmp.shared.data

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

@Serializable
class TestData(
    val userId: Int,
    val id: Int,
    val title: String,
    val completed: Boolean
)

@Serializable
class TestPostData(
    val userId: Int,
    val title: String,
    val body: String,
    val id: Int
) : BaseModel(){

    override fun toJson(): String = json.encodeToString(serializer(), this)

}

abstract class BaseModel{
    protected val json = Json { ignoreUnknownKeys = true }
    abstract fun toJson() : String
}
