package com.ezmobdev.weatariumkmp.shared.services.weather

import com.ezmobdev.weatariumkmp.shared.api.client.ApiClient
import com.ezmobdev.weatariumkmp.shared.api.request.Request
import com.ezmobdev.weatariumkmp.shared.data.WeatherRaw

class WeatherServiceImpl : WeatherService {

    companion object {
        private const val KEY = "7e18e01170ba9927bcd5d7f290e7ca86"
        private const val BASE_URL = "https://api.openweathermap.org/data/2.5/weather"
    }

    override suspend fun loadWeather(city: String): WeatherRaw =
        ApiClient.execute(weatherRawRequest(city))

    private fun weatherRawRequest(city: String): Request = Request.Get(BASE_URL).apply {
        appendQueryParams {
            put("units", "metric")
            put("appId", KEY)
            put("q", city)
        }
    }

}