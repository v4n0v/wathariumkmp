package com.ezmobdev.weatariumkmp.shared


import kotlin.coroutines.CoroutineContext

expect val uiDispatcher: CoroutineContext
expect val ioDispatcher: CoroutineContext

