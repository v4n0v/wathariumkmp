package com.ezmobdev.weatariumkmp.shared.api.request

import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

sealed class Request(
    val requestType: RequestType,
    val baseUrl: String,
) {
    var queryParams: MutableMap<String, String>? = null
        private set
    var headers: MutableMap<String, String>? = null
        private set
    var body: String? = null
        private set
    private val safeHeaders: MutableMap<String, String>
        get() = headers ?: hashMapOf<String, String>().also { headers = it }

    private val safeParams: MutableMap<String, String>
        get() = queryParams ?: hashMapOf<String, String>().also { queryParams = it }

    open class Get(
        url: String
    ) : Request(RequestType.GET, url)

    open class Post(
        url: String
    ) : Request(RequestType.POST, url)

    open class Put(
        url: String
    ) : Request(RequestType.PUT, url)

    open class Delete(
        url: String,
    ) : Request(RequestType.DELETE, url)

    @Suppress("NON_PUBLIC_CALL_FROM_PUBLIC_INLINE")
   inline fun <reified T> addBody(data: T){
       body = Json { ignoreUnknownKeys = true }.encodeToString(data)
    }

    //todo add url encoding
    fun appendQueryParams(bloc: QueryParams.() -> Unit) {
        bloc(safeParams)
    }

    fun appendHeaders(headers: Headers){
        safeHeaders.putAll(headers)
    }

    fun appendHeaders(vararg headers: Header) {
        headers.forEach { safeHeaders[it.first] = it.second }
    }

}

typealias QueryParams = MutableMap<String, String>
typealias Header = Pair<String, String>
typealias Headers = MutableMap<String, String>