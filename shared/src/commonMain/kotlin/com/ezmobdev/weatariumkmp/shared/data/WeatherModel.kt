package com.ezmobdev.weatariumkmp.shared.data

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json


@Serializable
class WeatherModel(
    val id: Int,
    val city: String,
    val temp: Double?,
    val minTemp: Double?,
    val maxTemp: Double?,
    val humidity: Int?,
    val pressure: Double?,
    val wind: Double?,
    val description: String?,
    val iconName: String?,
    val imageUrl: String?
) {
    companion object {
        private  val json = Json { ignoreUnknownKeys = true }
        fun fromJson(json: String): WeatherModel {
            return  Json.decodeFromString(serializer(), json)
        }
    }

    override fun toString(): String =
        "$city: $temp˚\n$description ($iconName)\n$imageUrl"

    fun toJson(): String = json.encodeToString(serializer(), this)

}