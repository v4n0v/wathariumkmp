package com.ezmobdev.weatariumkmp.shared

import com.ezmobdev.weatariumkmp.shared.api.Service
import com.ezmobdev.weatariumkmp.shared.services.image.FlickrImageService
import com.ezmobdev.weatariumkmp.shared.services.image.ImageService
import com.ezmobdev.weatariumkmp.shared.services.testService.TestDataService
import com.ezmobdev.weatariumkmp.shared.services.testService.TestServiceImpl
import com.ezmobdev.weatariumkmp.shared.services.weather.WeatherService
import com.ezmobdev.weatariumkmp.shared.services.weather.WeatherServiceImpl
import com.ezmobdev.weatariumkmp.shared.usecase.WeathariumUseCase
import com.ezmobdev.weatariumkmp.shared.usecase.WeathariumUseCaseImpl

object NetworkModule {

    private val services: MutableMap<String, Service> = hashMapOf()

    val testService: TestDataService by lazy { TestServiceImpl() }
    val imageService: ImageService by lazy { FlickrImageService() }
    val weatherService: WeatherService by lazy { WeatherServiceImpl() }
    val weathariumUseCase: WeathariumUseCase by lazy { WeathariumUseCaseImpl(imageService, weatherService) }
}