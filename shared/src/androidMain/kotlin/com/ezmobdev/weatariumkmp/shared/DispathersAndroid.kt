package com.ezmobdev.weatariumkmp.shared


import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext

actual val uiDispatcher: CoroutineContext
    get() = Dispatchers.Main

actual val ioDispatcher: CoroutineContext
    get() = Dispatchers.IO