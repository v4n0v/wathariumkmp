import com.ezmobdev.weatariumkmp.shared.api.request.Request
import com.ezmobdev.weatariumkmp.shared.data.TestPostData
import com.ezmobdev.weatariumkmp.shared.services.testService.TestServiceImpl
import kotlin.test.*


class RequestTest {

    companion object {
        private const val POST_URL = ""
        private const val HEADER_KEY = "Content"
        private const val HEADER_VALUE = "json"
        private const val BODY_JSON = """{"userId":1,"title":"title","body":"body","id":101}"""
        private   val POST_DATA = TestPostData(1, "title", "body", 101)
    }

    @Test
    fun requestBody() {
        val request = Request.Post(POST_URL)
            .apply {
                addBody(POST_DATA)
            }

        assertTrue {
            request.body == BODY_JSON
        }
    }

    @Test
    fun headers() {
        val request = Request.Get(POST_URL).apply {
            appendHeaders(HEADER_KEY to HEADER_VALUE)
        }
        assertTrue {
            request.headers?.size == 1 && request.headers?.get(HEADER_KEY) == HEADER_VALUE
        }
    }

    @Test
    fun toJson(){
        val json = POST_DATA.toJson()
        assertTrue {
            json == BODY_JSON
        }
    }

}