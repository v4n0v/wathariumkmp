package com.ezmobdev.weatariumkmp.shared


import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Runnable
import platform.darwin.*
import kotlin.coroutines.CoroutineContext
import kotlin.native.concurrent.freeze

actual val uiDispatcher: CoroutineContext
    get() =MainDispatcher// NsQueueDispatcher(dispatch_get_main_queue())

actual val ioDispatcher: CoroutineContext
    get() = MainDispatcher
//        NsQueueDispatcher(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT.toLong(),0))

internal class NsQueueDispatcher(private val dispatchQueue: dispatch_queue_t) : CoroutineDispatcher() {
    override fun dispatch(context: CoroutineContext, block: Runnable) =
        dispatch_async(dispatchQueue.freeze()) { block.run() }

}


private object  MainDispatcher: CoroutineDispatcher() {

    override fun dispatch(context: CoroutineContext, block: Runnable) {
        dispatch_async(dispatch_get_main_queue()) {
            try {
                block.run().freeze()
            } catch (err: Throwable) {
                throw err
            }
        }
    }
}