import org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTarget

plugins {
    kotlin("multiplatform")
    id("com.android.library")
//    id("kotlin-android-extensions")
//    id("kotlinx-serialization")
    kotlin("plugin.serialization") //version "1.4.30"
}

val serializationVersion: String by project
val reactiveVersion : String by project
val coroutinesVersion : String by project
val sqldelightVersion : String by project
val ktorVersion: String by project
val loggerVersion: String by project

kotlin {
    android()
    ios {
        binaries {
            framework {
                baseName = "shared"
            }
        }
     }
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation ("org.jetbrains.kotlin:kotlin-stdlib-common")
//                implementation ("org.jetbrains.kotlinx:kotlinx-serialization-runtime-common:$serializationVersion")
                implementation ("io.ktor:ktor-client-core:$ktorVersion")
                implementation ("io.ktor:ktor-client-json:$ktorVersion")
                implementation ("io.ktor:ktor-client-logging:$ktorVersion")
                implementation ("io.ktor:ktor-client-serialization:$ktorVersion")
                implementation("ch.qos.logback:logback-classic:$loggerVersion")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }
        val androidMain by getting {
            dependencies {
                implementation("com.google.android.material:material:1.2.1")
                implementation("org.jetbrains.kotlin:kotlin-stdlib")
//                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:$serializationVersion")
//
//                // Coroutines
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutinesVersion")
//
//                // HTTP
                implementation("io.ktor:ktor-client-android:$ktorVersion")
//                implementation ("org.jetbrains.kotlinx:kotlinx-serialization-json:$serializationVersion")
//                implementation("io.ktor:ktor-client-core-jvm:$ktorVersion")
//                implementation("io.ktor:ktor-client-json-jvm:$ktorVersion")
//                implementation("io.ktor:ktor-client-logging-jvm:$ktorVersion")
//                implementation("io.ktor:ktor-client-serialization-jvm:$ktorVersion")
            }
        }
        val androidTest by getting {
            dependencies {
                implementation(kotlin("test-junit"))
                implementation("junit:junit:4.13")
            }
        }
        val iosMain by getting {
            dependencies {
//                implementation ("org.jetbrains.kotlinx:kotlinx-serialization-runtime-native:$serializationVersion")
//
//                // Cache
//                implementation ("com.squareup.sqldelight:ios-driver:$sqldelightVersion")
//
//                // HTTP
                implementation ("io.ktor:ktor-client-ios:$ktorVersion")
//                implementation ("io.ktor:ktor-client-core-native:$ktorVersion")
//                implementation ("io.ktor:ktor-client-json-native:$ktorVersion")
//                implementation ("io.ktor:ktor-client-logging-native:$ktorVersion")
//                implementation ("io.ktor:ktor-client-serialization-native:$ktorVersion")
            }
        }
        val iosTest by getting
    }
}

android {
    compileSdkVersion(29)
    sourceSets["main"].manifest.srcFile("src/androidMain/AndroidManifest.xml")
    defaultConfig {
        minSdkVersion(24)
        targetSdkVersion(29)
    }
}

val packForXcode by tasks.creating(Sync::class) {
    group = "build"
    val mode = System.getenv("CONFIGURATION") ?: "DEBUG"
    val sdkName = System.getenv("SDK_NAME") ?: "iphonesimulator"
    val targetName = "ios" + if (sdkName.startsWith("iphoneos")) "Arm64" else "X64"
    val framework = kotlin.targets.getByName<KotlinNativeTarget>(targetName).binaries.getFramework(mode)
    inputs.property("mode", mode)
    dependsOn(framework.linkTask)
    val targetDir = File(buildDir, "xcode-frameworks")
    from({ framework.outputDirectory })
    into(targetDir)
}

tasks.getByName("build").dependsOn(packForXcode)